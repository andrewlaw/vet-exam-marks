#!/usr/bin/perl -w

package marksWorkbook;

use strict;

use warnings;

use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;

# ---------------------------------------------------------------------------
# 
# Workbook object


sub new {
    my ($class, @ARGS) = (@_);
    my $self = {
        _bookname     => $ARGS[0],
        _modulename   => $ARGS[1],
        _modulecode   => $ARGS[2],
        _session      => $ARGS[3],
        _studentfile  => $ARGS[4],
    };
    bless $self, $class;

    my @sections = ();
    $self->{_sections} = \@sections;
    
    my %formatters = ();
    $self->{_formatters} = \%formatters;
    
    
    my (%students) = ();
    open SFILE, $self->{_studentfile} || die "Opening student file list";
	while (defined (my $line = <SFILE>)) {
		chomp ($line);
		my ($exam, $uun, $firstname, $surname) = split("[\t,]", $line);
		$students{$exam}{'uun'} = $uun;
		$students{$exam}{'firstname'} = $firstname;
		$students{$exam}{'surname'} = $surname;
	
	}
	$self->{_students} = \%students;
	close SFILE;

	my $workbook = Excel::Writer::XLSX->new( $self->{_bookname} );
	$self->{_workbook} = $workbook;

	my $summary = new summaryWorksheet('Overall', $self);
	$self->{_summary} = $summary;
	
	$self->__buildFormatters();
	
    return $self;
}



sub __buildFormatters() {
    my( $self ) = @_;
	
	my $workbook = $self->getWorkbook();

# Header Format (bold, centred)
	my $headerFormat
			= $workbook->add_format(
					bold     => 1,
				);
	$headerFormat->set_align( 'center' );
	$headerFormat->set_align( 'vcenter' );
	$self->{_formatters}{'headerFormat'} = $headerFormat;

# 'Marks per question' and 'exclude Question' formats
# (grey, lt grey background, italic, centred, small)
	my $maxMarksFormat = $workbook->add_format(
		color => '#A6A6A6',
		bg_color => '#D9D9D9',
		italic => 1,
		size => 8,
		align => 'center',
	);
	$self->{_formatters}{'maxMarksFormat'} = $maxMarksFormat;

	$self->{_formatters}{'excludeQuestionFormat'} = $maxMarksFormat;

# Formatting for the sheet-based percentage cells. Going to use
# 2 decimal places, red, bold, centred
	my $percentCellFormat = $workbook->add_format(
		color => 'red',
		bold => 1,
		align => 'center',
		num_format => '0.00',
	);
	$self->{_formatters}{'percentCellFormat'} = $percentCellFormat;

# Formatting for the summary weighted percentage cells. Going to use
# 2 decimal places, purple, bold, centred
	my $weightedPercentCellFormat = $workbook->add_format(
		color => 'purple',
		bold => 1,
		align => 'center',
		num_format => '0.00',
	);
	$self->{_formatters}{'weightedPercentCellFormat'} = $weightedPercentCellFormat;

# Formatting for the corrected score cells. Going to use
# 3 decimal places, green, bold, centred
	my $correctedScoreFormat = $workbook->add_format(
		color => 'green',
		bold => 1,
		align => 'center',
		num_format => '0.000',
	);
	$self->{_formatters}{'correctedScoreFormat'} = $correctedScoreFormat;

# Formatting for the rounded score cells. Going to use
# 0 decimal places, brown, bold, centred
	my $roundedFormat = $workbook->add_format(
		color => 'brown',
		bold => 1,
		align => 'center',
		num_format => '0',
	);
	$self->{_formatters}{'roundedFormat'} = $roundedFormat;

# Bold and right-formatted
	my $rightBoldFormat = $workbook->add_format(
		bold => 1,
		align => 'right',
	);
	$self->{_formatters}{'rightBoldFormat'} = $rightBoldFormat;

# Error format - red background
	my $errorFormat = $workbook->add_format(
		bold => 1,
		bg_color => '#FF4433',
	);
	$self->{_formatters}{'errorFormat'} = $errorFormat;

# Hidden format - white text
	my $hideDataFormat = $workbook->add_format(
		color => 'white',
	);
	$self->{_formatters}{'hideDataFormat'} = $hideDataFormat;

# Blank Cell format - orangish
	my $blankCellFormat = $workbook->add_format(
		bg_color => '#EEBB99',
		border_color => '#D0D7E5',,
		border => 7,
	);
	$self->{_formatters}{'blankCellFormat'} = $blankCellFormat;

}



sub dump {
    my( $self ) = @_;
	printf("Saving as workbook '%s'\n", $self->{_bookname});
	
	$self->fillDetails();
	
	$self->{_workbook}->close();
}


sub fillDetails {
    my( $self ) = @_;

	foreach my $section (@{$self->{_sections}}) {
		printf("With Section '%s'\n", $section->{_sheetname});
		$self->stampModule($section);
		$section->fillDetails();
	}
	
	my $summary = $self->{_summary};
	$self->stampModule($summary);
	$summary->fillDetails();
}


sub stampModule {
    my( $self, $section ) = @_;
    
    my $sheet = $section->{_worksheet};
    
    my $rightBoldFormat = $self->getFormatStyle('rightBoldFormat');
    
	$sheet->write( "A1", "Code:", $rightBoldFormat );
	$sheet->write( "B1", $self->getModuleCode() );

	$sheet->write( "A2", "Name:", $rightBoldFormat );
	$sheet->write( "B2", $self->getModuleName() );

	$sheet->write( "A3", "Session:", $rightBoldFormat );
	$sheet->write( "B3", $self->getAcademicSession() );

	my @studentList = $self->getStudentExamNumbers();
	$sheet->write( "A4", "Cohort:", $rightBoldFormat );
	$sheet->write( "B4", scalar(@studentList) . " students");
    
}


sub getReferenceExamNumberRange {
    my( $self ) = @_;
    
    return $self->{_summary}->getReferenceExamNumberRange();
}

# Add a new section

sub addNewSection {
    my( $self, $sheetname, $questionCount,$maxMarks, $weighting ) = (@_);
    
    my $newsection
    	= new marksWorksheet($sheetname, $questionCount,$maxMarks, $weighting, $self);
    
    push(
    	@{$self->{_sections}}, 
    	$newsection
    );
}


sub getFormatStyle {
	my ($self, $formatName) = (@_);
	
	my %formatters = $self->getFormatters();
	
	return $formatters{$formatName};
}



# Get the underlying workbook
sub getWorkbook {
    my( $self ) = @_;
	return $self->{_workbook};
}

sub getModuleCode {
    my( $self ) = @_;
	return $self->{_modulecode};
}
sub getModuleName {
    my( $self ) = @_;
	return $self->{_modulename};
}
sub getAcademicSession {
    my( $self ) = @_;
	return $self->{_session};
}
sub getStudentExamNumbers {
	my ($self) = (@_);
	my %students = %{$self->{_students}};
	return sort keys %students;
}
sub getStudentList {
	my ($self) = (@_);
	my %students = %{$self->{_students}};
	return %students;
}
sub getFormatters {
	my ($self) = (@_);
	my %formatters = %{$self->{_formatters}};
	return %formatters;
}
sub getSections {
	my ($self) = (@_);
	my @sections = @{$self->{_sections}};
	return @sections;
}

1;
