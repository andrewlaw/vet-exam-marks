#!/usr/bin/perl -w

use strict;

use warnings;

use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;

use marksWorksheet;
use marksWorkbook;


my $UPPER_HEADER_ROW_INDEX = 5;
my $MAX_MARKS_ROW_INDEX = 6;
my $EXCLUDE_Q_ROW_INDEX = 7;
my $HEADER_ROW_INDEX = 8;

my $UUN_COL_INDEX = 0;
my $SURNAME_COL_INDEX = 1;
my $FIRSTNAME_COL_INDEX = 2;
my $EXAM_NUMBER_COLUMN = 3;

my $remainingProportion = 100;

my %{students} = ();




my $moduleName = prompt("Module name ?", "Animal Body Module 2");
my $moduleCode = prompt("Module code ?", "GEP BVMS08058");
my $session = prompt("Academic session ?", "2016-2017");
my $studentFile = prompt("File with student details ?", "student-list.txt");
my $name = prompt("Name to save the file under ?", "testing.xlsx");

my $markWorkbook = new marksWorkbook (
	$name,
	$moduleName,
	$moduleCode,
	$session,
	$studentFile
	);


LOOP:
while (1) {
	my $sectionName = prompt("Name for next exam section (blank to finish) ?", "");
	if ($sectionName eq "") {
		last LOOP;
	}
	
	my $questionCount = prompt("How many questions in this section ?", 5);
	my $marksPerQuestion = prompt("How many marks per question in this section ?", 10);
	my $weight = prompt(
			"What overall weighting is applied to the mark from this section ?",
			$remainingProportion);
	
	$remainingProportion -= $weight;
	
	$markWorkbook->addNewSection(
		$sectionName,
		$questionCount,
		$marksPerQuestion,
		$weight);
	
}




$markWorkbook->dump();

# ---------

sub prompt {
   my ($text, $default) = @_;
   print $text . " [" . $default . "] ";
 
   my $answer = <STDIN>;
   chomp $answer;
   if ($answer eq "") {
	   $answer = $default;
   }
   return $answer;
}

