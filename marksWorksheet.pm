#!/usr/bin/perl -w


use strict;

use warnings;

use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;
use summaryWorksheet;

package marksWorksheet;
our @ISA = ('summaryWorksheet');

my $EXAM_NUMBER_COLUMN = 0;

my $HEADER_ROW_INDEX = 8;
my $EXCLUDE_Q_ROW_INDEX = $HEADER_ROW_INDEX - 1;
my $MAX_MARKS_ROW_INDEX = $HEADER_ROW_INDEX - 2;


# ---------------------------------------------------------------------------
# 
# Worksheet object

sub new {
    my (
    	$class,
    	$sheetname,
    	$questionCount,
    	$maxMarks,
    	$weighting,
    	$parent
    ) = (@_);
    
    my $self = summaryWorksheet->new( $sheetname, $parent);
    
	$self->{_questionCount} = $questionCount;
	$self->{_maxMarks} = $maxMarks;
	$self->{_proportion} = $weighting;
    
    bless $self, $class;
    return $self;
}


sub doHeaders {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};
	my $questionCount = $self->{_questionCount};

	my $headerFormat = $self->getFormatStyle('headerFormat');
	my $excludeQuestionFormat = $self->getFormatStyle('excludeQuestionFormat');
	my $maxMarksFormat = $self->getFormatStyle('maxMarksFormat');

	$worksheet->write($MAX_MARKS_ROW_INDEX, $EXAM_NUMBER_COLUMN,
							"Max", $maxMarksFormat);
	$worksheet->write($EXCLUDE_Q_ROW_INDEX, $EXAM_NUMBER_COLUMN,
							"Exclude->", $excludeQuestionFormat);	
	$worksheet->write($HEADER_ROW_INDEX, $EXAM_NUMBER_COLUMN,
				'Exam No', $headerFormat);

	my $maxMarks = $self->{_maxMarks};

	for (my $n = 1; $n <= $questionCount; $n++) {
		
		my $column = $EXAM_NUMBER_COLUMN + $n;
		
		$worksheet->write($HEADER_ROW_INDEX, $column,
				'Q'.$n, $headerFormat);
		$worksheet->write( $MAX_MARKS_ROW_INDEX, $column,
							$maxMarks, $maxMarksFormat);
		$worksheet->write( $EXCLUDE_Q_ROW_INDEX, $column,
							"", $excludeQuestionFormat);
		$worksheet->write($HEADER_ROW_INDEX - 3, $column,
				'Q'.$n, $headerFormat);
	}

	my $totalColumn = $EXAM_NUMBER_COLUMN + $questionCount + 1;
	$worksheet->write($HEADER_ROW_INDEX, $totalColumn,
				'Total', $headerFormat);
	$worksheet->write($HEADER_ROW_INDEX - 3, $totalColumn,
				'Total', $headerFormat);
	my $percentageColumn = $totalColumn + 1;
	$worksheet->write($HEADER_ROW_INDEX, $percentageColumn,
				'%age', $headerFormat);
	
	
	my $maxMarksFormula = $self->getTotalFormula($MAX_MARKS_ROW_INDEX);
	
	$worksheet->write_formula( $MAX_MARKS_ROW_INDEX, $totalColumn,
				$maxMarksFormula, $maxMarksFormat);
	$worksheet->write( $EXCLUDE_Q_ROW_INDEX, $totalColumn,
						"", $excludeQuestionFormat);
	
}


sub getExcludeQuestionRange {
	my ($self) = (@_);
	my $questionCount = $self->{_questionCount};
	
	my $ifRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$EXCLUDE_Q_ROW_INDEX,
			$EXCLUDE_Q_ROW_INDEX,
			$EXAM_NUMBER_COLUMN + 1,
			$EXAM_NUMBER_COLUMN + $questionCount,
			1,
			1,
			1,
			1);

	return $ifRange;	
}


sub getMarksSumRange {
	my ($self, $rowNum) = (@_);
	my $questionCount = $self->{_questionCount};
	
	my $sumRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$rowNum,
			$rowNum,
			$EXAM_NUMBER_COLUMN + 1,
			$EXAM_NUMBER_COLUMN + $questionCount);
	
	return $sumRange;
}


sub getMarksReferenceRange {
	my ($self) = (@_);
	my $questionCount = $self->{_questionCount};
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();
	my $sheetname = $self->{_sheetname};

	my $marksReferenceRange
		= sprintf(
			"'%s'!%s",
			$sheetname,
			Excel::Writer::XLSX::Utility::xl_range(
				$HEADER_ROW_INDEX + 1,
				$HEADER_ROW_INDEX + scalar(@students),
				$EXAM_NUMBER_COLUMN + $questionCount + 2,
				$EXAM_NUMBER_COLUMN + $questionCount + 2,
				1,
				1,
				1,
				1
			)
		);
	
	return $marksReferenceRange;
}
sub getExamNumberRange {
	my ($self) = (@_);
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();
	my $sheetname = $self->{_sheetname};

	my $examNumberRange
		= sprintf(
			"'%s'!%s",
			$sheetname,
			Excel::Writer::XLSX::Utility::xl_range(
				$HEADER_ROW_INDEX + 1,
				$HEADER_ROW_INDEX + scalar(@students),
				$EXAM_NUMBER_COLUMN,
				$EXAM_NUMBER_COLUMN,
				1,
				1,
				1,
				1
			)
		);
	
	return $examNumberRange;
}


sub getTotalFormula {
	my ($self, $rowNum) = (@_);
	
	my $totalFormula
		= sprintf(
			"=SUMIFS(%s, %s, \"\")",
			$self->getMarksSumRange($rowNum),
			$self->getExcludeQuestionRange()
		);
	return $totalFormula;
}

sub getPercentageFormula {
	my ($self, $rowNum) = (@_);
	
	my $questionCount = $self->{_questionCount};
	my $totalColumn = $EXAM_NUMBER_COLUMN + $questionCount + 1;

	my $thisTotalCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$rowNum,
			$totalColumn
		);
	
	my $maxTotalCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$MAX_MARKS_ROW_INDEX,
			$totalColumn,
			1,
			1
		);
	
	my $percentageFormula
		= sprintf(
			"=%s*100/%s",
			$thisTotalCell,
			$maxTotalCell
		);
	return $percentageFormula;
}

sub doStudentRows {
	my ($self) = (@_);
	
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();
	my $worksheet = $self->{_worksheet};
	my $rowNum = $HEADER_ROW_INDEX;
	# and total marks formula
	my $questionCount = $self->{_questionCount};
	my $totalColumn = $EXAM_NUMBER_COLUMN + $questionCount + 1;
	my $percentageColumn = $totalColumn + 1;

	foreach my $examNo (sort @students) {
		$rowNum++;
		# fill in the student's exam number
		$worksheet->write($rowNum, $EXAM_NUMBER_COLUMN,
								$examNo);
		$worksheet->write_formula( $rowNum, $totalColumn,
			$self->getTotalFormula($rowNum));
		$worksheet->write_formula( $rowNum, $percentageColumn,
			$self->getPercentageFormula($rowNum),
			$self->getFormatStyle('percentCellFormat'));
	}

}


sub addConditionalFormatting {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};
	my $questionCount = $self->{_questionCount};
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();

	my $invalidScoreFormat = $self->getFormatStyle('errorFormat');
	my $excludeQuestionFormat = $self->getFormatStyle('excludeQuestionFormat');
	my $blankCellFormat = $self->getFormatStyle('blankCellFormat');

	# look for duplicate exam number entries
	my $duplicateScoresBlock
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN,
			$EXAM_NUMBER_COLUMN + $questionCount + 2,
			0,
			0,
			0,
			0);
	my $examNumberRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN,
			$EXAM_NUMBER_COLUMN,
			1,
			1,
			1,
			1);
	my $firstExamNumberCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$HEADER_ROW_INDEX + 1,
			$EXAM_NUMBER_COLUMN,
			0,
			1
		);
	
	$worksheet->conditional_formatting(
		$duplicateScoresBlock,
		{
			type     => 'formula',
			criteria => "=COUNTIF($examNumberRange,$firstExamNumberCell)>1",
			format   => $invalidScoreFormat,
			stop_if_true => 1,
		});
		

	# look for exam number entries that don't match the defined set
	my $canonicalExamNumberRange
		= $parent->getReferenceExamNumberRange();

	$worksheet->conditional_formatting(
		$duplicateScoresBlock,
		{
			type     => 'formula',
			criteria => "=ISERROR(MATCH($firstExamNumberCell,$canonicalExamNumberRange,0))",
			format   => $invalidScoreFormat,
			stop_if_true => 1,
		});
		
	# grey out scores that are for questions that are excluded
	my $excludeQuestionCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$EXCLUDE_Q_ROW_INDEX,
			$EXAM_NUMBER_COLUMN + 1,
			1,
			0
		);
	
	my $greyScoresRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN + 1,
			$EXAM_NUMBER_COLUMN + $questionCount,
			0,
			0,
			0,
			0);
	$worksheet->conditional_formatting(
		$greyScoresRange,
		{
			type     => 'formula',
			criteria => "=NOT(ISBLANK($excludeQuestionCell))",
			format   => $excludeQuestionFormat,
			stop_if_true => 1,
		});
	
	# highlight scores that are too high or low for the question
	my $maxScoreCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$MAX_MARKS_ROW_INDEX,
			$EXAM_NUMBER_COLUMN + 1,
			1,
			0
		);
	
	my $scoresRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN + 1,
			$EXAM_NUMBER_COLUMN + $questionCount,
			0,
			0,
			0,
			0);
	$worksheet->conditional_formatting(
		$scoresRange,
		{
			type     => 'cell',
			criteria => 'not between',
			minimum  => 0,
			maximum  => $maxScoreCell,
			format   => $invalidScoreFormat,
			stop_if_true => 1,
		});
	
	# highlight blank scores
	$worksheet->conditional_formatting(
		$scoresRange,
		{
			type     => 'blanks',
			format   => $blankCellFormat,
			stop_if_true => 1,
		});
	
}



1;
