#!/usr/bin/perl -w

use strict;

use warnings;

use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;


package summaryWorksheet;

my $STUDENT_NUMBER_COLUMN = 0;
my $FIRST_NAME_COLUMN = 1;
my $SURNAME_COLUMN = 2;
my $EXAM_NUMBER_COLUMN = 3;
my $HEADER_ROW_INDEX = 8;
my $SHOW_NAMES_ROW_INDEX = $HEADER_ROW_INDEX - 2;
my $CONVERSION_TABLE_HEADER_ROW_INDEX = 1;

my $DEFAULT_ZERO_MARK = 0;
my $DEFAULT_PASS_MARK = 50;
my $DEFAULT_EXCELLENT_MARK = 70;
my $DEFAULT_MAX_MARK = 100;
# ---------------------------------------------------------------------------
# 
# General Summary Worksheet object


sub new {
    my (
    	$class,
    	$sheetname,
    	$parent
    ) = (@_);
    my $self = {
        _sheetname     => $sheetname,
        _parent        => $parent
    };
    
    my $workbook = $parent->getWorkbook();
    $self->{_worksheet} = $workbook->add_worksheet($sheetname);

    bless $self, $class;
    return $self;
}


# Fill in the data for this sheet
sub fillDetails {
	my ($self) = (@_);
	
	$self->doHeaders();
	$self->doStudentRows();
	$self->addConditionalFormatting();

}


sub doHeaders {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};
	my $headerFormat = $self->getFormatStyle('headerFormat');
    my $rightBoldFormat = $self->getFormatStyle('rightBoldFormat');
	my $parent = $self->{'_parent'};
    my $workbook = $parent->getWorkbook();

	# student number
	$worksheet->write($HEADER_ROW_INDEX, $STUDENT_NUMBER_COLUMN,
							"UUN", $headerFormat);
	
	# first name
	$worksheet->write($HEADER_ROW_INDEX, $FIRST_NAME_COLUMN,
							"First Name", $headerFormat);
	
	# surname
	$worksheet->write($HEADER_ROW_INDEX, $SURNAME_COLUMN,
							"Surname", $headerFormat);
	
	# fill in the student's exam number
	$worksheet->write($HEADER_ROW_INDEX, $EXAM_NUMBER_COLUMN,
							"Exam No", $headerFormat);

	# Indicate the proportion row
	$worksheet->write($HEADER_ROW_INDEX - 1, $EXAM_NUMBER_COLUMN,
							"Weighting", $headerFormat);

	# Prompt the show/hide flag
	$worksheet->write($SHOW_NAMES_ROW_INDEX, 0,
							"Names?", $rightBoldFormat);

	# Fill in each section header in turn
	my @sections = $parent->getSections();
	my $count = 0;
	my @weightCells = ();
	foreach my $section (@sections) {
		my $baseColumn = $EXAM_NUMBER_COLUMN + ($count * 2) + 1;
		
		$worksheet->merge_range(
				$HEADER_ROW_INDEX,
				$baseColumn,
				$HEADER_ROW_INDEX,
				$baseColumn + 1,
				$section->{_sheetname},
				$headerFormat );
		$worksheet->write($HEADER_ROW_INDEX - 1, $baseColumn + 1,
								$section->{_proportion}, $headerFormat);
		my $weightCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$HEADER_ROW_INDEX - 1,
				$baseColumn + 1,
				1,
				1
			);
		push(@weightCells, $weightCell);
		$count++;
	}
	
	# and then the total and pass/fail column
	my $totalColumn = $EXAM_NUMBER_COLUMN + (scalar(@sections) * 2) + 1;
	my $correctedColumn = $totalColumn + 1;
	my $ratioColumn = $totalColumn + 2;
	my $passFailColumn = $totalColumn + 3;
	$worksheet->write($HEADER_ROW_INDEX, $totalColumn,
							"Total", $headerFormat);
	my $totalWeightFormula
		= sprintf(
			"=SUM(%s)",
			join(",", @weightCells)
		);
	$worksheet->write_formula(
					$HEADER_ROW_INDEX - 1,
					$totalColumn,
					$totalWeightFormula,
					$headerFormat
				);
							
	$worksheet->write($HEADER_ROW_INDEX, $correctedColumn,
							"Corrected", $headerFormat);

	$worksheet->write($HEADER_ROW_INDEX, $ratioColumn,
							"Rounded", $headerFormat);

	$worksheet->write($HEADER_ROW_INDEX, $passFailColumn,
							"Pass/Fail", $headerFormat);
	$worksheet->write($HEADER_ROW_INDEX - 1, $passFailColumn,
							$DEFAULT_PASS_MARK, $headerFormat);
	my $passFailScoreCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$HEADER_ROW_INDEX - 1,
			$passFailColumn,
			1,
			1
		);

	# and the score conversion table
	$worksheet->write($CONVERSION_TABLE_HEADER_ROW_INDEX, $totalColumn - 1,
							"Scores", $headerFormat);
	$worksheet->write($CONVERSION_TABLE_HEADER_ROW_INDEX, $totalColumn,
							"This Exam", $headerFormat);
	$worksheet->write($CONVERSION_TABLE_HEADER_ROW_INDEX, $correctedColumn,
							"Corrected", $headerFormat);
	$worksheet->write($CONVERSION_TABLE_HEADER_ROW_INDEX, $ratioColumn,
							"Ratio", $headerFormat);
							
	
	my @rowTitles = qw( Min: Pass: Excellent: Max:);
	my @values = (
		$DEFAULT_ZERO_MARK,
		$DEFAULT_PASS_MARK,
		$DEFAULT_EXCELLENT_MARK,
		$DEFAULT_MAX_MARK
	);
	
	my $convertRow = $totalColumn - 1;
	for (my $row = 0; $row < 4; $row++) {
		my $thisRow = $CONVERSION_TABLE_HEADER_ROW_INDEX + $row + 1;
		$worksheet->write(
					$thisRow,
					$totalColumn - 1,
					$rowTitles[$row],
					$rightBoldFormat);
		$worksheet->write(
					$thisRow,
					$totalColumn,
					$values[$row]);
		$worksheet->write(
					$thisRow,
					$correctedColumn,
					$values[$row]);
		if ($row > 0) {
			my $thisCorrectedCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$thisRow,
					$correctedColumn,
					1,
					1
				);
			my $previousCorrectedCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$thisRow - 1,
					$correctedColumn,
					1,
					1
				);
			my $thisScoreCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$thisRow,
					$totalColumn,
					1,
					1
				);
			my $previousScoreCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$thisRow - 1,
					$totalColumn,
					1,
					1
				);
			my $ratioFormula =
				sprintf(
					"=(%s-%s)/(%s-%s)",
					$thisCorrectedCell,
					$previousCorrectedCell,
					$thisScoreCell,
					$previousScoreCell
				);
			$worksheet->write_formula(
							$thisRow,
							$ratioColumn,
							"=$ratioFormula"
						);
			
		}
	}

	$worksheet->write_formula(
					$CONVERSION_TABLE_HEADER_ROW_INDEX + 2,
					$correctedColumn,
					"=$passFailScoreCell"
				);

}


sub doStudentRows {
	my ($self) = (@_);
	
	my $parent = $self->{_parent};
	my %students = $parent->getStudentList();
	my $worksheet = $self->{_worksheet};
	my $rowNum = $HEADER_ROW_INDEX;
	foreach my $examNo (sort keys %students) {
		$rowNum++;
		# student number
		$worksheet->write($rowNum, $STUDENT_NUMBER_COLUMN,
								$students{$examNo}{'uun'});
		
		# first name
		$worksheet->write($rowNum, $FIRST_NAME_COLUMN,
								$students{$examNo}{'firstname'});
		
		# surname
		$worksheet->write($rowNum, $SURNAME_COLUMN,
								$students{$examNo}{'surname'});
		
		# fill in the student's exam number
		$worksheet->write($rowNum, $EXAM_NUMBER_COLUMN,
								$examNo);
	}

	# Fill in each the copied value formula and the weighted
	# calculation value for each section header in turn
	my $percentCellFormat = $self->getFormatStyle('percentCellFormat');
	my $weightedPercentCellFormat = $self->getFormatStyle('weightedPercentCellFormat');
	my $correctedScoreFormat = $self->getFormatStyle('correctedScoreFormat');
	my $roundedFormat = $self->getFormatStyle('roundedFormat');
	my @sections = $parent->getSections();
	my $sectionCount = scalar(@sections);
	my $totalWeightCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$HEADER_ROW_INDEX - 1,
			$EXAM_NUMBER_COLUMN + ($sectionCount * 2) + 1,
			1,
			1
		);
	my @columns = ();
	my $count = 0;
	foreach my $section (@sections) {
		my $baseColumn = $EXAM_NUMBER_COLUMN + ($count * 2) + 1;
		
		push (@columns, $baseColumn + 1);
		
		my $marksRange = $section->getMarksReferenceRange();
		my $examNoLookupRange = $section->getExamNumberRange();
		
		my $weightCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$HEADER_ROW_INDEX - 1,
				$baseColumn + 1,
				1,
				1
			);
		
		my $rowNum = $HEADER_ROW_INDEX;
		foreach my $examNo (sort keys %students) {
			$rowNum++;
			my $examNumberCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$rowNum,
					$EXAM_NUMBER_COLUMN
				);
			my $lookupFormula
				= sprintf(
					"=INDEX(%s,MATCH(%s,%s,0))",
					$marksRange,
					$examNumberCell,
					$examNoLookupRange
				);
			$worksheet->write_formula(
				$rowNum,
				$baseColumn,
				$lookupFormula,
				$percentCellFormat
			);
			
			my $scoreCell
				= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
					$rowNum,
					$baseColumn
				);
			my $weightedFormula
				= sprintf(
					"=(%s*%s)/%s",
					$scoreCell,
					$weightCell,
					$totalWeightCell
				);
			$worksheet->write_formula(
				$rowNum,
				$baseColumn + 1,
				$weightedFormula,
				$weightedPercentCellFormat
			);

		}
		$count++;
	}
	
	# Now do the summation of the different parts
	$rowNum = $HEADER_ROW_INDEX;
	my $totalColumn = $EXAM_NUMBER_COLUMN + (scalar(@sections) * 2) + 1;
	my $correctedColumn = $totalColumn + 1;
	my $roundedColumn =  $totalColumn + 2;
	my $passFailColumn = $totalColumn + 3;
	
	my $passMarkCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$rowNum - 1,
			$passFailColumn,
			1,
			1
		);
	my $headerFormat = $self->getFormatStyle('headerFormat');
	foreach my $examNo (sort keys %students) {
		$rowNum++;
		my @sumCells = ();
		foreach my $column (@columns) {
			push (@sumCells, Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$rowNum,
				$column
				)
			);
		}
		my $sumFormula
			= sprintf(
				"=SUM(%s)",
				join(",", @sumCells)
			);
		$worksheet->write_formula(
			$rowNum,
			$totalColumn,
			$sumFormula,
			$percentCellFormat
		);
		
		my $totalCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$rowNum,
				$totalColumn
			);
		
		my $passMarkCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 2,
				$totalColumn,
				1, 1
			);
		my $passRatioCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 2,
				$totalColumn + 2,
				1, 1
			);
		my $correctedPassMarkCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 2,
				$totalColumn + 1,
				1, 1
			);
		my $excellentMarkCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 3,
				$totalColumn,
				1, 1
			);
		my $excellentRatioCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 3,
				$totalColumn + 2,
				1, 1
			);
		my $correctedExcellentMarkCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 3,
				$totalColumn + 1,
				1, 1
			);
		my $maxRatioCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$CONVERSION_TABLE_HEADER_ROW_INDEX + 4,
				$totalColumn + 2,
				1, 1
			);
		
		my $correctionFormula
			= sprintf(
				"=IF(%s<=%s,%s*%s,if(%s<=%s,%s+((%s-%s)*%s),%s+((%s-%s)*%s)))",
				$totalCell,
				$passMarkCell,
				$totalCell,
				$passRatioCell,
				$totalCell,
				$excellentMarkCell,
				$correctedPassMarkCell,
				$totalCell,
				$passMarkCell,
				$excellentRatioCell,
				$correctedExcellentMarkCell,
				$totalCell,
				$excellentMarkCell,
				$maxRatioCell
				);
		$worksheet->write_formula(
			$rowNum,
			$correctedColumn,
			$correctionFormula,
			$correctedScoreFormat
		);
		
		
		my $correctedCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$rowNum,
				$correctedColumn
			);
		my $roundingFormula
			= sprintf(
				"=ROUND(%s,0)",
				$correctedCell
			);
		$worksheet->write_formula(
			$rowNum,
			$roundedColumn,
			$roundingFormula,
			$roundedFormat
		);
		
		
		my $roundedCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$rowNum,
				$roundedColumn
			);
		my $passFailScoreCell
			= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
				$HEADER_ROW_INDEX - 1,
				$passFailColumn,
				1,
				1
			);
		my $passFailFormula
			= sprintf(
				"=IF(%s >= %s, \"Pass\", \"Fail\")",
				$roundedCell,
				$passFailScoreCell
			);
		$worksheet->write_formula(
			$rowNum,
			$passFailColumn,
			$passFailFormula,
			$headerFormat
		);
	}
	
}

sub getFormatStyle {
	my ($self, $formatName) = (@_);
	
	my $parent = $self->{_parent};
	return $parent->getFormatStyle($formatName);
}


sub getReferenceExamNumberRange {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};

	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();

	my $examNumberRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN,
			$EXAM_NUMBER_COLUMN,
			1,
			1,
			1,
			1);
	return "'".$self->{_sheetname}."'!$examNumberRange";
}


sub addConditionalFormatting {
	my ($self) = (@_);
	
	$self->_showHideNameFormatting();
	$self->_duplicateExamNumberFormatting();
}


sub _showHideNameFormatting {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();
	my $hideDataFormat = $self->getFormatStyle('hideDataFormat');
	
	# Hide names etc. unless the trigger cell says "Show"
	my $triggerCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$SHOW_NAMES_ROW_INDEX,
			1,
			1,
			1
		);
	
	my $detailsCellRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			0,
			$EXAM_NUMBER_COLUMN - 1,
			0,
			0,
			0,
			0);
	$worksheet->conditional_formatting(
		$detailsCellRange,
		{
			type     => 'formula',
			criteria => "=NOT(EXACT($triggerCell,\"Show\"))",
			format   => $hideDataFormat,
		});
	
}


sub _duplicateExamNumberFormatting {
	my ($self) = (@_);
	my $worksheet = $self->{_worksheet};
	my $parent = $self->{_parent};
	my @students = $parent->getStudentExamNumbers();
	my @sections = $parent->getSections();
	my $invalidScoreFormat = $self->getFormatStyle('errorFormat');

	# look for duplicate exam number entries
	my $duplicateScoresBlock
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			0,
			$EXAM_NUMBER_COLUMN + (scalar(@sections) * 2) + 3,
			0,
			0,
			0,
			0);
	my $examNumberRange
		= Excel::Writer::XLSX::Utility::xl_range(
			$HEADER_ROW_INDEX + 1,
			$HEADER_ROW_INDEX + scalar(@students),
			$EXAM_NUMBER_COLUMN,
			$EXAM_NUMBER_COLUMN,
			1,
			1,
			1,
			1);
	my $firstExamNumberCell
		= Excel::Writer::XLSX::Utility::xl_rowcol_to_cell(
			$HEADER_ROW_INDEX + 1,
			$EXAM_NUMBER_COLUMN,
			0,
			1
		);
	
	$worksheet->conditional_formatting(
		$duplicateScoresBlock,
		{
			type     => 'formula',
			criteria => "=COUNTIF($examNumberRange,$firstExamNumberCell)>1",
			format   => $invalidScoreFormat,
			stop_if_true => 1,
		});
		
}

1;
